DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
while true; do
    read -p "[Warning!!] Please make sure the robot is not running (robot stop), and the robot's power cable is plugged into the outlet. Otherwise this firmware upgrade may brick the gripper's motor control board. Do you want to proceed? (y/N)" yn
    case $yn in
        [Yy]* ) pr2-grant $DIR/tools/fwprog -iecat0 $DIR/firmware/wg006_gripper_E_3.209_rev2032.bit -p25;
                pr2-grant $DIR/tools/fwprog -iecat0 $DIR/firmware/wg006_gripper_E_3.209_rev2032.bit -p34; break;;
        [Nn]* ) exit;;
        * ) echo "Please answer yes(Y) or no(N).";;
    esac
done
