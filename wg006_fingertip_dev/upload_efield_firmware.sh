DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
$DIR/picoasm/picoasm -i $DIR/firmware/efield_fw.s -H $DIR/firmware/efield_fw.hex
pr2-grant $DIR/tools/picoblaze_prog -r $DIR/firmware/efield_fw.hex -iecat0 -p25 -W
pr2-grant $DIR/tools/picoblaze_prog -r $DIR/firmware/efield_fw.hex -iecat0 -p34 -W
