DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
$DIR/picoasm/picoasm -i $DIR/firmware/pressure_fw.s -H $DIR/firmware/pressure_fw.hex
pr2-grant $DIR/tools/picoblaze_prog -r $DIR/firmware/pressure_fw.hex -iecat0 -p25 -W
pr2-grant $DIR/tools/picoblaze_prog -r $DIR/firmware/pressure_fw.hex -iecat0 -p34 -W
